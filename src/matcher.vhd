-------------------------------------------------------------------------
-- INTERFACE PACKAGE: Defines types used in the module's ports
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.matcher_pkg.all;
use work.matcher_queue_interface_pkg.all;

package matcher_interface_pkg is
  
  type SLsegProfile_t is record
    highq     : std_logic;
    valid     : std_logic;
  end record;
  
  constant SLSEGPROFILE_NULL : SLsegProfile_t := (
    highq     => '0',
    valid     => '0'
  );
  
  type SLseg_arr_t is array(natural range <>) of SLsegProfile_t;
  
  type matcher_in_t is record
    new_wave  : std_logic;
    sls       : SLseg_arr_t(1 downto 0); 
  end record;

  constant MATCHER_IN_NULL : matcher_in_t := (
    new_wave  => '0',
    sls       => (others => SLSEGPROFILE_NULL)
  );

  type  matcher_queue_out_arr_t   is array(natural range <>) of matcher_queue_out_t;

end package;


-------------------------------------------------------------------------
-- ENTITY
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.matcher_pkg.all;
use work.matcher_interface_pkg.all;
use work.matcher_queue_interface_pkg.all;

-- For a detailed explanation on how it works, see matcher_pkg.vhd
entity matcher is
  port(
    clk               : in  std_logic;
    rst               : in  std_logic;
    input             : in  matcher_in_t;
    q_data            : out matcher_queue_out_arr_t(N_PRIORITIES-1 downto 0); 
    q_rden            : in  std_logic_vector       (N_PRIORITIES-1 downto 0)
	);
end entity;

architecture Behavioural of matcher is

  type queue_in_arr_t is array(N_PRIORITIES-1 downto 0) of matcher_queue_in_t;  
  signal queue_in_arr   : queue_in_arr_t;

begin

  ---------------------------------------------------------------
  -- Input stage and matching matrix, generates inputs for array of queues
  --
  input_to_queues_gen: if True generate
  
    ---------------------------------------
    -- Types and constants
    --

    type waveSLprofiles_t is array(WAVE_SIZE - 1 downto 0) of SLsegProfile_t;
    constant WAVESLPROFILES_NULL : waveSLprofiles_t := (others => SLSEGPROFILE_NULL);
    
    type waveProfiles_t is array(1 downto 0) of waveSLprofiles_t;
    constant WAVEPROFILES_NULL : waveProfiles_t := (others => WAVESLPROFILES_NULL);
    
    type mm_WaveProfiles_t is array(CURR_AND_PAST_WAVES_TOTAL-1 downto 0) of waveProfiles_t;
    constant MM_WAVEPROFILES_NULL : mm_WaveProfiles_t := (others => WAVEPROFILES_NULL);
    
    type indexes_arr_t is array(integer range <>) of unsigned(SUBWAVES_NUM_BITS-1 downto 0);
    ---------------------------------------
    -- Registers
    --
    type reg_t is record
      -- input stage
      rx_inpt_index       : unsigned(SUBWAVES_NUM_BITS-1 downto 0);
      rx_waveProfiles     : waveProfiles_t;
      rx_done             : std_logic;
      
      -- work data for the matching matrix
      mm_new_wave         : std_logic;
      mm_waveProfiles     : mm_WaveProfiles_t; --indexes are (wave age)(superlayer)(index in wave)
      mm_indexes          : indexes_arr_t(WAVESLPROFILES_NULL'range);
      
      -- output
      o_queue_in_arr      : queue_in_arr_t;
    end record;
    
    constant REG_NULL : reg_t := (
      rx_inpt_index       => (others => '0'),
      rx_waveProfiles     => WAVEPROFILES_NULL,
      rx_done             => '0',
      
      mm_new_wave         => '0',
      mm_waveProfiles     => MM_WAVEPROFILES_NULL,
      mm_indexes          => (others => (others => '0')),
      
      o_queue_in_arr      => (others => MATCHER_QUEUE_IN_NULL)
    );
    
    signal reg, nextReg : reg_t := REG_NULL;
    
    ---------------------------------------
    -- Functions
    --
    
    function get_priority_onehot (
      seg0 : SLsegProfile_t;
      seg1 : SLsegProfile_t)
    return std_logic_vector is
      variable prio1hot : std_logic_vector(N_PRIORITIES-1 downto 0) := (others => '0');
    begin
      if seg0.valid = '1' and seg1.valid = '1' then
        if    seg0.highq = '1' and seg1.highq = '1' then prio1hot(2) := '1';
        elsif seg0.highq = '1' or  seg1.highq = '1' then prio1hot(1) := '1';
        else                                             prio1hot(0) := '1';
        end if;
      end if;
      return prio1hot;
    end function;
    
  begin
  
    -- Main state machine
    process(reg, input)
      variable sec_wave_i : natural range 0 to CURR_AND_PAST_WAVES_TOTAL-1;
      variable sec_sl_i   : natural range 0 to 1;
      variable sec_seg_i  : natural range 0 to WAVE_SIZE-1;
    begin
      -- Default values.
      nextReg <= reg;
      
      ---------------------------
      -- Input stage
      ---------------------------
      -- input index goes from 0 to WAVE_SIZE-1 and resets to 0, waiting for next wave
      nextReg.rx_done <= '0';
      if reg.rx_inpt_index = to_unsigned(WAVE_SIZE-1, reg.rx_inpt_index'length) then
        nextReg.rx_inpt_index <= (others => '0');
        nextReg.rx_done <= '1';
      elsif reg.rx_inpt_index > 0 or input.new_wave = '1' then
        nextReg.rx_inpt_index <= reg.rx_inpt_index + 1;
      end if;
      
      -- store segment profile to its index
      if reg.rx_inpt_index > 0 or input.new_wave = '1' then
        for sl_i in reg.rx_waveProfiles'range loop
            nextReg.rx_waveProfiles(sl_i) <= input.sls(sl_i) & reg.rx_waveProfiles(sl_i)(WAVE_SIZE-1 downto 1);
        end loop;
      end if;
      
      -- shift the pipeline of bx waveprofiles and kickstart new matching wave
      nextReg.mm_new_wave <= reg.rx_done;
      
      if reg.rx_done = '1' then
        for i in reg.mm_indexes'range loop
          nextReg.mm_indexes(i) <= to_unsigned(i,reg.mm_indexes(i)'length);
        end loop;
        nextReg.mm_waveProfiles <= reg.mm_waveProfiles(MM_WAVEPROFILES_NULL'high-1 downto 0) & reg.rx_waveProfiles;
      elsif reg.mm_new_wave = '1' or reg.mm_indexes(0) > 0 then
        -- Each subwave cycle, all waves are rotated to place the "working segment" in the bottom-most position
        nextReg.mm_indexes <= reg.mm_indexes(0) & reg.mm_indexes(WAVE_SIZE-1 downto 1);
        for wave_i in reg.mm_waveProfiles'range loop
          for sl_i in reg.mm_waveProfiles(wave_i)'range loop
            nextReg.mm_waveProfiles(wave_i)(sl_i) <=
              reg.mm_waveProfiles(wave_i)(sl_i)(0) &
              reg.mm_waveProfiles(wave_i)(sl_i)(WAVE_SIZE-1 downto 1);
          end loop;
        end loop;
      end if;
      
      ---------------------------
      -- Matching Matrix processing
      ---------------------------
      for q_i in N_PRIORITIES-1 downto 0 loop
  
        nextReg.o_queue_in_arr(q_i).new_wave    <= reg.mm_new_wave;
        if reg.mm_new_wave = '1' or reg.mm_indexes(0) > 0 then
          nextReg.o_queue_in_arr(q_i).valid       <= '1';
        else
          nextReg.o_queue_in_arr(q_i).valid       <= '0';
        end if;
        nextReg.o_queue_in_arr(q_i).subwave_i   <= reg.mm_indexes(0);
  
        for pair_subwave_i in SUBWAVE_PAIRS_SIZE-1 downto 0 loop
          sec_wave_i  := (pair_subwave_i + WAVE_SIZE) / (WAVE_SIZE*2);
          sec_sl_i    := (pair_subwave_i / WAVE_SIZE) mod 2;
          sec_seg_i   := pair_subwave_i mod WAVE_SIZE;

          nextReg.o_queue_in_arr(q_i).subwave_pairs(pair_subwave_i) <=
            get_priority_onehot (
                reg.mm_waveProfiles(         0)(1-sec_sl_i)(        0),
                reg.mm_waveProfiles(sec_wave_i)(  sec_sl_i)(sec_seg_i)
              )(q_i);
        end loop;
      end loop;
  
      ---------------------------
      -- Output
      ---------------------------
      queue_in_arr <= reg.o_queue_in_arr;
  
    end process;
    
    process(clk)
    begin
      if rising_edge(clk) then
        if rst = '1' then 
          reg <= REG_NULL;
        else
          reg <= nextReg;
        end if;
      end if;
    end process;
  
  end generate;

  ---------------------------------------
  -- Array of per-priority queues
  --

  per_priority_gen : for q_i in 0 to N_PRIORITIES-1 generate
    
    matcher_queue_inst : entity work.matcher_queue
    port map(
      clk       => clk,
      rst       => rst,
      input     => queue_in_arr  (q_i),
      output    => q_data (q_i),
      q_rden    => q_rden (q_i)
    );
  
  end generate;

end architecture;
