library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.matcher_pkg.all;
use work.matcher_interface_pkg.all;

entity matcher_tb is
end entity;

architecture behavioral of matcher_tb is

  constant LHC_CLK_RATIO  : natural := 6;
  constant LHC_CLK_PERIOD : time := 25 ns;
  constant CLK_PERIOD     : time := LHC_CLK_PERIOD / LHC_CLK_RATIO;
  
  signal clk                : std_logic := '1';
  signal rst                : std_logic := '1';
--  signal bunch_ctr          : unsigned(11 downto 0) := (others => '0');
  signal matcher_in         : matcher_in_t := MATCHER_IN_NULL;
  signal matcher_q_data     : matcher_queue_out_arr_t(N_PRIORITIES-1 downto 0);
  signal matcher_q_rden     : std_logic_vector       (N_PRIORITIES-1 downto 0) := (others => '1');

begin

  matcher_inst: entity work.matcher
  port map(
    clk               => clk                ,
    rst               => rst                ,
    input             => matcher_in         ,
    q_data            => matcher_q_data     ,
    q_rden            => matcher_q_rden
  );



  clk   <= not  clk after (  clk_period / 2.0 );
  
  process
  begin
    rst <= '1';
    matcher_in <= MATCHER_IN_NULL;

    wait for (CLK_PERIOD * (LHC_CLK_RATIO * 2 + 1));

    rst <= '0';

    wait for (CLK_PERIOD * LHC_CLK_RATIO);

    -- WAVE 13
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 14
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 15
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    matcher_in.sls(1) <= (valid => '1', highq => '1');
    wait for (clk_period);
    matcher_in.sls(1) <= SLSEGPROFILE_NULL;
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 16
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    matcher_in.sls(1) <= (valid => '1', highq => '1');
    wait for (clk_period);
    matcher_in.sls(1) <= SLSEGPROFILE_NULL;
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 17
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    matcher_in.sls(0) <= (valid => '1', highq => '1');
    matcher_in.sls(1) <= (valid => '1', highq => '0');
    wait for (clk_period);
    matcher_in.sls(0) <= (valid => '1', highq => '0');
    matcher_in.sls(1) <= (valid => '1', highq => '1');
    wait for (clk_period);
    matcher_in.sls(0) <= SLSEGPROFILE_NULL;
    matcher_in.sls(1) <= SLSEGPROFILE_NULL;
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 18
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 19
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 20
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);

    -- WAVE 21
    matcher_in.new_wave <= '1';
    wait for (clk_period);
    matcher_in.new_wave <= '0';
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period);
    wait for (clk_period * 6);



    wait;
    
  end process;

end architecture;

