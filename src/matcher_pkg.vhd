library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package matcher_pkg is

  -------------------------------------------------------------------------------
  -- Constants that determine the data flow of the matcher
  -- 
  
  constant CLK_RATIO : natural := 12;
  
  constant N_PRIORITIES : natural := 3;
  constant WAVE_SIZE    : natural := 6;
  constant WAVE_SIZE_BITS : natural := 3;
  
  constant PAST_WAVES_NUM            : natural := 2 ;
  constant CURR_AND_PAST_WAVES_TOTAL : natural := 1 + PAST_WAVES_NUM ;
  
  constant W2W_XINGS          : natural := 2 * CURR_AND_PAST_WAVES_TOTAL - 1;
  constant SUBWAVE_PAIRS_SIZE : natural := WAVE_SIZE * W2W_XINGS; -- 30
  constant SUBWAVE_PAIRS_BITS : natural := 5;
  constant SUBWAVES_NUM       : natural := WAVE_SIZE; -- 6
  constant SUBWAVES_NUM_BITS  : natural := 3;

  constant QUEUE_SIZE       : natural := 16;

  constant WAVE_MAX_NUM_HOLDS : natural := CLK_RATIO - WAVE_SIZE; --6
  constant WAVE_HOLD_CTR_BITS : natural := 3;
  
  constant SUBSUBWAVE_SIZE  : natural := 2;   -- must be a power of 2
  constant SUBSUBWAVE_SIZE_BITS : natural := 1;


  -------------------------------------------------------------------------------
  -- Some useful type definitions
  -- 
  type match_enc_t is record
    subwave_i : unsigned(SUBWAVES_NUM_BITS-1 downto 0);
    pair_i    : unsigned(SUBWAVE_PAIRS_BITS-1 downto 0);
    valid     : std_logic;
  end record;
  constant MATCH_ENC_NULL : match_enc_t := (
    subwave_i => (others => '0'),
    pair_i    => (others => '0'),
    valid     => '0'
  );
  -- An array of the aforementioned encoded pairing informations for the sub-sub-wave and the queue
  type match_enc_arr_t is array(integer range <>) of match_enc_t;
  
end package;

