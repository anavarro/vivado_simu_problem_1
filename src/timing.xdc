## 160 MHz
#create_clock -period 6.25 -name clk [get_ports clk]
## 200 MHz
#create_clock -period 5.00 -name clk [get_ports clk]
## 250 MHz (6xLHC + margin)
#create_clock -period 4.00 -name clk [get_ports clk]
## 320 MHz
#create_clock -period 3.125 -name clk [get_ports clk]
## 360 MHz (9xLHC)
#create_clock -period 2.770 -name clk [get_ports clk]
## 360 MHz (9xLHC + margin)
#create_clock -period 2.650 -name clk [get_ports clk]
## 425 MHz
#create_clock -period 2.35 -name clk [get_ports clk]
## 455 MHz
#create_clock -period 2.2 -name clk [get_ports clk]
## 480 MHz (12xLHC)
#create_clock -period 2.079 -name clk [get_ports clk]
## 500 MHz
#create_clock -period 2.00 -name clk [get_ports clk]
## 525 MHz
create_clock -period 1.9 -name clk [get_ports clk]
## 533 MHz
#create_clock -period 1.875 -name clk [get_ports clk]
## 555 MHz
#create_clock -period 1.8 -name clk [get_ports clk]
## 588 MHz
#create_clock -period 1.7 -name clk [get_ports clk]

